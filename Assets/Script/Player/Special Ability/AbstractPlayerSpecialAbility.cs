﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// classe astratta che rappresenta una generica abilità speciale del giocatore;
/// </summary>
public abstract class AbstractPlayerSpecialAbility : MonoBehaviour {

	/// <summary>
	/// si occupa di settare i parametri dello script del giocatore copiandoli dallo script di partenza
	/// la sua implementazione deve occuparsi di copiare i valori nello script,
	/// ricevendo come parametro uno script dello stesso tipo contenente i valori corretti;
	/// in caso i valori siano oggetti è necessario copiarli, non assegnare il puntatore
	/// </summary>
	/// <param name="origin">script di partenza dal quale copiare i parametri</param>
	public abstract void GetParameterFromScript(AbstractPlayerSpecialAbility origin);

	/// <summary>
	/// si occupa di distruggere l'abilità quando termina
	/// </summary>
	public abstract void RemoveAbility();



}
