﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// SpecialAbility che implementa il volo sul posto orizzontale
/// </summary>
public class HorizontalFly : AbstractPlayerSpecialAbility {

	/// <summary>
	/// abilita o disabilita il termine del abilità in base al tempo trascorso da quando la ottieni
	/// </summary>
	public bool disableByTimerAbsolute = false;
	/// <summary>
	/// timer che segna il tempo trascorso da quando si ottiene l'abilità
	/// </summary>
	public Timer expiredAbsolute = new Timer(60, 0);
	/// <summary>
	/// abilita o disabilita il termine del abilità in base al tempo trascorso durante l'utilizzo
	/// </summary>
	public bool disableByTimerRelative = false;
	/// <summary>
	/// timer che segna il tempo di utilizzo del abilità
	/// </summary>
	public Timer expiredRelative = new Timer(10, 0);
	/// <summary>
	/// abilita o disabilita il termine del abilità in base al numero di attivazioni
	/// </summary>
	public bool disableByUse = true;
	/// <summary>
	/// numero di attivazioni restanti
	/// </summary>
	public int useRemain = 5;

	/// <summary>
	/// indica se l'abilità è in uso o meno
	/// </summary>
	public bool used = false;

	/// <summary>
	/// puntatore allo script PlayerStandardMovement
	/// </summary>
	protected PlayerStandardMovement standardMovement;


	#region Override
	/// <summary>
	/// TIPO: HorizontalFly - 
	/// si occupa di settare i parametri dello script del giocatore copiandoli dallo script di partenza
	/// </summary>
	/// <param name="origin">TIPO: HorizontalFly, script di partenza dal quale copiare i parametri</param>
	public override void GetParameterFromScript(AbstractPlayerSpecialAbility origin) {
		HorizontalFly tmpOrgin = origin as HorizontalFly;
		disableByTimerAbsolute = tmpOrgin.disableByTimerAbsolute;
		expiredAbsolute = new Timer(tmpOrgin.expiredAbsolute.GetMaxTime(), 0f);
		disableByTimerRelative = tmpOrgin.disableByTimerRelative;
		expiredRelative = new Timer(tmpOrgin.expiredRelative.GetMaxTime(),0f);
		disableByUse = tmpOrgin.disableByUse;
		useRemain = tmpOrgin.useRemain;
	}

	//BUG: "standardMovement.rb2d.constraints = RigidbodyConstraints2D.None;" non ha alcun effetto se richiamata qui, da sistemare
	public override void RemoveAbility() {
		standardMovement.rb2d.constraints = RigidbodyConstraints2D.FreezeRotation;
		Destroy(this);
	}

	#endregion


	#region Awake & Start
	/// <summary>
	/// inizializza standardMovement
	/// </summary>
	void Awake () {
		standardMovement = this.GetComponent<PlayerStandardMovement>();
	}

	#endregion


	#region Update & FixedUpdate
	/// <summary>
	/// controlla se l'abilità va rimossa;
	/// se non è cosi controlla se viene premuto il tasto che la attiva e eventualmente la applica
	/// </summary>
	void Update() {
		//controlla se l'abilità va rimossa
		if (disableByUse && useRemain <= 0)
			RemoveAbility();
		else if (disableByTimerAbsolute && expiredAbsolute.Check())
			RemoveAbility();
		else if (disableByTimerRelative && expiredRelative.Check())
			RemoveAbility();

		//applica l'abilità
		if (Input.GetButton("Horizontal Fly")) {
			standardMovement.rb2d.constraints = RigidbodyConstraints2D.FreezePositionY | RigidbodyConstraints2D.FreezeRotation;
			expiredRelative.Advance();
			used = true;
		}
		else {      //previene casi ambigui in cui il buff non dovrebbe essere attivo
					//esempio, se si cambia finestra, si smette di premere il tasto e si ritorna al gioco
			standardMovement.rb2d.constraints = RigidbodyConstraints2D.FreezeRotation;
			if (used) {
				useRemain--;
				used = false;
			}

		}

		expiredAbsolute.Advance();
	}

	#endregion


}
