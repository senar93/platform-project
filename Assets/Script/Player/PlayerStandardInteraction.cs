﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Script che controlla le interazioni standard del giocatore con l'ambiente
/// (tendenzialmente entrare in un area con un certo tag)
/// </summary>
public class PlayerStandardInteraction : MonoBehaviour {

	/// <summary>
	/// attiva o disattiva la possibilità del giocatore di essere ucciso dagli oggetti con tag "DeathStandard"
	/// </summary>
	public bool canBeKill = true;



	/// <summary>
	/// controlla se il giocatore attiva qualche tipo di trigger e se si esegue l'azione appropriata
	/// </summary>
	/// <param name="other">oggetto che ha attivato il trigger</param>
	void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.tag == "DeathStandard" && canBeKill) {
			LevelManager.levelManagerPointer.Death();
		} else if(other.gameObject.tag == "DeathAbsolute") {
			LevelManager.levelManagerPointer.Death();
		} else if (other.gameObject.tag == "Win") {
			LevelManager.levelManagerPointer.Win();
		} else if (other.gameObject.tag == "CheckPoint") {
			GameManager.gameManagerPointer.customSpawnPoint = true;
			GameManager.gameManagerPointer.spawnCoord = other.transform.position;
		}
	}


}

