﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HORROR_FIX : MonoBehaviour {


	// Update is called once per frame
	void Update () {
		HorizontalFlyFix();
	}



	#region HorizontalFly FIX
	[SerializeField] private bool horizontalFlyFixEnable = true;
	private bool horizontalFlyScriptExpired = false;
	private bool horizontalFlyScriptDetected = false;
	private HorizontalFly horizontalFlyScript;

	private void HorizontalFlyFix() {
		if (horizontalFlyFixEnable)
		{
			if (horizontalFlyScriptExpired)
			{
				this.GetComponent<PlayerStandardMovement>().rb2d.constraints = RigidbodyConstraints2D.FreezeRotation;
				horizontalFlyScriptExpired = false;
			}
			else if (horizontalFlyScriptDetected)
			{
				if (this.GetComponent<HorizontalFly>() == null)
				{
					horizontalFlyScriptExpired = true;
					horizontalFlyScriptDetected = false;
				}
			}
			else if (this.GetComponent<HorizontalFly>() != null)
				horizontalFlyScriptDetected = true;
		}
	}
	#endregion



}
