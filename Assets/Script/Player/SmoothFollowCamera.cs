﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Script che permette alla Main Camera di seguire l'oggetto a cui è associato
/// la main camera per essere riconosciuta deve essere taggata MainCamera, e non devono esserci altri oggetti con questo tag nella scena
/// </summary>
public class SmoothFollowCamera : MonoBehaviour {

	/// <summary>
	/// velocità di movimento della camera
	/// </summary>
	public float smoothSpeed = 4f;
	/// <summary>
	/// distanza oltre la quale la camera inizia ad accellerare proporzionalmente alla distanza
	/// </summary>
	public float distanceSpeedUp = 4f;
	public Vector2 offset;

	/// <summary>
	/// puntatore alla Main Camera
	/// </summary>
	protected GameObject cameraObj;
	/// <summary>
	/// coordinate da raggiungere
	/// </summary>
	protected Vector3 specificVector;
	/// <summary>
	/// distanza attuale tra oggetto e camera
	/// </summary>
	protected float actDist = 0;




	/// <summary>
	/// individua la Main Camera
	/// </summary>
	public void Start() {
		cameraObj = LevelManager.mainCamera;
	}


	/// <summary>
	/// a ogni Update sposta la Main Camera in base ai parametri del oggetto e alla sua posizione rispetto a quella della Main Camera
	/// </summary>
	public void Update() {
		specificVector = new Vector3(LevelManager.player.transform.position.x + offset.x,
									 LevelManager.player.transform.position.y + offset.y,
									 cameraObj.transform.position.z);
		actDist = Vector2.Distance(LevelManager.player.transform.position, cameraObj.transform.position);
		if (actDist > distanceSpeedUp)
			cameraObj.transform.position = Vector3.Lerp(cameraObj.transform.position, specificVector, smoothSpeed * Time.deltaTime * (1 + actDist));
		else
			cameraObj.transform.position = Vector3.Lerp(cameraObj.transform.position, specificVector, smoothSpeed * Time.deltaTime);
	}


}
