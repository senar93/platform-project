﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Script che consente al giocatore di effettuare i movimenti standard (movimento orizzontale e salto)
/// </summary>
public class PlayerStandardMovement: MonoBehaviour {

	/// <summary>
	/// attiva o disattiva la possibilità di saltare
	/// </summary>
	public bool jumpEnable = true;
	/// <summary>
	/// massimo numero di doppi salti effettuabili durante un singolo salto;
	/// 0 disabilita la possibilità di fare il doppio salto
	/// </summary>
	public int maxMultipleJumpNumber = 0;
	/// <summary>
	/// tempo minimo tra un salto e l'altro (doppi salti compresi)
	/// </summary>
	public Timer jumpDelay = new Timer(0.5f,0f);
	/// <summary>
	/// forza applicata al salto
	/// </summary>
	public float jumpForce = 1000f;
	/// <summary>
	/// forza applicata al movimento orizzontale
	/// </summary>
	public float moveForce = 365f;
	/// <summary>
	/// velocità massima raggiungibile
	/// </summary>
	public float maxSpeed = 5f;

	/// <summary>
	/// indica se l'oggetto si trova o no al suolo
	/// </summary>
	public bool grounded = false;
	/// <summary>
	/// indica se l'oggetto sta saltando
	/// </summary>
	public bool jump = false;
	/// <summary>
	/// indica il numero di doppi salti effettuati,
	/// si resetta toccando terra
	/// </summary>
	[SerializeField] protected int multipleJumpCounter = 0;

	/// <summary>
	/// puntatore al Rigidbody2D del oggetto
	/// </summary>
	[HideInInspector] public Rigidbody2D rb2d;
	/// <summary>
	/// puntatore alla posizione nel quale calcolare del il pg sta toccando o meno terra (piedi)
	/// </summary>
	protected  Transform groundCheck;
	/// <summary>
	/// comando di movimento sul asse orizzontale
	/// </summary>
	protected float horizontalAxis;



	#region Awake & Start
	/// <summary>
	/// inizializza rb2d e groundCheck;
	/// l'oggetto DEVE contenere un oggetto di nome GroundCheck per permettere l'inizializzazione automatica
	/// </summary>
	void Awake () {
		rb2d = GetComponent<Rigidbody2D>();
		foreach(Transform child in transform) {				//autoset groundCheck
			if (child.name == "GroundCheck") {
				groundCheck = child;
				break;										//dovrebbe uscire dal foreach non dal if con questo break
			}
		}
	}

	#endregion


	#region Update & FixedUpdate
	/// <summary>
	/// controlla gli input, imposta i flag jump e grounded, imposta jumpDelay e applica la forza in caso si salti
	/// </summary>
	void Update () {
		grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));
		horizontalAxis = Input.GetAxis("Horizontal");
		jumpDelay.Advance();

		if (Input.GetButton("Jump") && jumpEnable) {
			if (grounded) {
				rb2d.velocity = new Vector2 (rb2d.velocity.x, 0);
				jump = true;
				multipleJumpCounter = 0;
				jumpDelay.Reset();
			} else {
				if (multipleJumpCounter < maxMultipleJumpNumber && jumpDelay.Check()) {
					rb2d.velocity = new Vector2 (rb2d.velocity.x, 0);
					multipleJumpCounter++;
					jump = true;
					jumpDelay.Reset();
				}
			}
		}

	}

	/// <summary>
	/// esegue il movimento orizzontale e il salto in caso siano stati dati gli input appropriati
	/// </summary>
	void FixedUpdate() {
		
		if (horizontalAxis == 0) {
			rb2d.velocity = new Vector2 (0,rb2d.velocity.y);
		} else {
			if (Mathf.Sign(horizontalAxis) * rb2d.velocity.x < maxSpeed)
				rb2d.AddForce (Vector2.right * Mathf.Sign(horizontalAxis) * moveForce);
			
			if (Mathf.Abs (rb2d.velocity.x) > maxSpeed)
				rb2d.velocity = new Vector2 (Mathf.Sign (rb2d.velocity.x) * maxSpeed, rb2d.velocity.y);
		}


		if (jump) {
			rb2d.AddForce (new Vector2 (0f, jumpForce));
			jump = false;
		}

	}

	#endregion


}