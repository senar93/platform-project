﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// generico Timer utilizzato per contare il tempo trascorso
/// </summary>
[System.Serializable]
public class Timer {
	
	/// <summary>
	/// tempo trascorso
	/// </summary>
	[SerializeField] protected float actTime = 0;
	/// <summary>
	/// limite del timer, oltre il quale il Check restituisce true
	/// </summary>
	[SerializeField] protected float maxTime = 0;



	/// <summary>
	/// costruttore del oggetto Timer
	/// </summary>
	/// <param name="endTime">limite del timer</param>
	/// <param name="startTime">tempo iniziale, di default 0</param>
	public Timer(float endTime, float startTime = 0) {
		SetMaxTime(endTime);
		Reset(startTime);
	}



	/// <summary>
	/// restituisce il tempo trascorso da quando il timer è stato avviato
	/// </summary>
	/// <returns>tempo trascorso da quando il timer è stato avviato</returns>
	public float GetActTime() {
		return actTime;
	}

	/// <summary>
	/// restituisce il limite del timer
	/// </summary>
	/// <returns>limite del timer</returns>
	public float GetMaxTime() {
		return maxTime;
	}

	/// <summary>
	/// imposta il limite del timer a un nuovo valore
	/// </summary>
	/// <param name="value">nuovo limite del timer</param>
	public void SetMaxTime(float value) {
		maxTime = value;
	}

	/// <summary>
	/// reimposta il tempo trascorso da quando è stato attivato il timer
	/// </summary>
	/// <param name="startTime">nuovo valore del tempo trascorso, di default 0</param>
	public void Reset(float startTime = 0) {
		actTime = startTime;
	}

	/// <summary>
	/// avanza il timer;
	/// permette al utilizzatore del timer di controllare quando far avanzare il timer
	/// (per esempio solo quando il giocatore si trova su una piattaforma fragile)
	/// </summary>
	public void Advance() {
		if (actTime <= maxTime)
			actTime += Time.deltaTime;
	}

	/// <summary>
	/// controlla se il limite del timer è stato raggiunto,
	/// se si restituisce TRUE, altrimenti FALSE
	/// </summary>
	/// <returns>TRUE se il timer è scaduto,
	///			 FALSE altrimenti</returns>
	public bool Check() {
		if (actTime >= maxTime)
			return true;
		return false;
	}


}
