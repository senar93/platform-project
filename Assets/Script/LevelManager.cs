﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// classe che contiene informazioni sulla partita in corso e a cui qualsiasi oggetto può avere accesso
/// DEVE essere univoca
/// </summary>
public class LevelManager : MonoBehaviour {

	/// <summary>
	/// puntatore statico al unico oggetto di tipo LevelManager presente di volta in volta in partita
	/// </summary>
	public static LevelManager levelManagerPointer;
	public static GameObject player;
	public static GameObject mainCamera;

	public string sceneThisName;
	/// <summary>
	/// abilita o disabilita un timer, allo scadere del tempo il giocatore perde automaticamente
	/// </summary>
	public bool haveLevelTimer = true;
	/// <summary>
	/// tempo trascorso dal inizio della partita
	/// </summary>
	public Timer levelTime = new Timer(99999999, 0);

	/// <summary>
	/// punteggio attuale
	/// </summary>
	public float score = 0;


	/// <summary>
	/// imposta la variabile statica levelManagerPointer per puntare a questo oggetto
	/// </summary>
	public void Awake() {
		//set static
		LevelManager.levelManagerPointer = this;
		player = GameObject.FindGameObjectWithTag("Player");
		mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
		//set GameManager value
		GameManager.gameManagerPointer.sceneLose = sceneThisName;
		//set other value
		if (GameManager.gameManagerPointer.customSpawnPoint) {
			player.transform.position = GameManager.gameManagerPointer.spawnCoord;
			mainCamera.transform.position = new Vector3(GameManager.gameManagerPointer.spawnCoord.x, GameManager.gameManagerPointer.spawnCoord.y, -10f);
		}
	}

	/// <summary>
	/// avanza il timer ad ogni update, e controlla se il tempo è finito o meno
	/// </summary>
	public void Update() {
		levelTime.Advance();
		if (haveLevelTimer && levelTime.Check())
			this.Death();
	}


	/// <summary>
	/// funzione richiamata al momento della morte del giocatore
	/// </summary>
	public void Death() {
		SceneManager.LoadScene(GameManager.gameManagerPointer.sceneLose);
	}

	/// <summary>
	/// funzione richiamata al momento della vittoria del giocatore
	/// </summary>
	public void Win() {
		int tmpIndex = GameManager.gameManagerPointer.sceneLevel.FindIndex(x => x == sceneThisName);
		if(GameManager.gameManagerPointer.scoreLevel[tmpIndex] < score)
			GameManager.gameManagerPointer.scoreLevel[tmpIndex] = score;
		GameManager.gameManagerPointer.customSpawnPoint = false;
		SceneManager.LoadScene(GameManager.gameManagerPointer.sceneWin);
	}


}
