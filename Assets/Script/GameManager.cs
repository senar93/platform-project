﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	public static GameManager gameManagerPointer;

	public bool customSpawnPoint = false;
	public Vector2 spawnCoord;

	public string sceneMainMenu;
	public string sceneInGameMenu;
	public string sceneLose;
	public string sceneWin;
	public List<string> sceneLevel;
	public List<bool> unlockedLevel;
	public List<float> scoreLevel;

	void Awake() {
		GameManager.gameManagerPointer = this;
		SceneManager.LoadScene(sceneMainMenu);
		if (sceneLevel.Count != unlockedLevel.Count || sceneLevel.Count != scoreLevel.Count)
			Debug.Log("<color=red>ERRORE:</color> le liste riguardanti le scene devono avere lo stesso numero di elementi");
	}


}
