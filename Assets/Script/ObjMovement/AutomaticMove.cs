﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutomaticMove : MonoBehaviour {

	public bool enabledFlag = true;
	public AbstractMovementPath path;
	public AbstractTrigger starterEvent;
	public bool alwaysCheckTriggerEvent = false;
	public float speed = 5f;


	// Use this for initialization
	void Start () {
		if (path == null)
			Destroy(this);
	}
	
	// Update is called once per frame
	void Update () {
		if (starterEvent == null)
			enabledFlag = true;
		else
			enabledFlag = starterEvent.IsActive(alwaysCheckTriggerEvent);

		if(enabledFlag)
			this.transform.position = Vector3.MoveTowards(this.transform.position, path.GetPath(this.transform.position), speed * Time.deltaTime);
	}

}
