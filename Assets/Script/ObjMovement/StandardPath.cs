﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandardPath : AbstractMovementPath {

	[SerializeField] protected float detectPointDistance = 0.5f;
	[SerializeField] protected List<GameObject> pathPoint;
	/// <summary>
	/// indica se è abilitato il restart oppure no
	/// </summary>
	[SerializeField] protected bool restartPath = false;
	/// <summary>
	/// TRUE: riparte dalla posizione di indice "restartPointPosition"
	/// FALSE: procede a ritroso seguendo la lista al contrario fino a "restartPointBackLimit"
	/// </summary>
	[SerializeField] protected bool restartFromPosition = false;
	[SerializeField] protected int restartPointPosition = 0;
	[SerializeField] protected int restartPointBackLimit = 0;

	protected int pointIndex = 0;
	protected bool restartingBackFlag = false;
	protected bool pathEndFlag = false;


	public void Initialize() {
		pathPoint = new List<GameObject>();
	}

	public void AddPoint(GameObject newPoint) {
		pathPoint.Add(newPoint);
	}

	public int PointListLenght() {
		return pathPoint.Count;
	}

	public Vector2 GetPointCoord(int i) {
		if (i < pathPoint.Count)
			return pathPoint[i].transform.position;
		else
			return pathPoint[pathPoint.Count - 1].transform.position;
	}

	public bool IsPathEnd() {
		return pathEndFlag;
	}


	public override Vector3 GetPath(Vector2 actPosition) {
		
		if(Vector2.Distance(actPosition,GetPointCoord(pointIndex)) <= detectPointDistance) {
			//caso generico
			if (restartingBackFlag && restartPath)
				pointIndex--;
			else
				pointIndex++;
			
			//raggiunto limite superiore lista
			if (PointListLenght() <= pointIndex) {
				if (restartPath)
					if (restartFromPosition)
						pointIndex = restartPointPosition;
					else {
						pointIndex--;
						restartingBackFlag = true;
					}
				else
					pathEndFlag = true;
			}

			//raggiunto limite inferiore lista
			if(pointIndex < restartPointBackLimit) {
				pointIndex++;
				restartingBackFlag = false;
			}

		}

		return GetPointCoord(pointIndex);

	}


}
