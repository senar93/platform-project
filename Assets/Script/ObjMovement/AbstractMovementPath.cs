﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractMovementPath : MonoBehaviour {

	public abstract Vector3 GetPath(Vector2 actPosition);
	public Vector3 GetPath(Vector3 actPosition) {
		return GetPath(new Vector2(actPosition.x, actPosition.y));
	}
	public Vector3 GetPath(float x, float y) {
		return GetPath(new Vector2(x, y));
	}


}
