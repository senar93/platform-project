﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoneDetectionTrigger : AbstractTrigger {

	[SerializeField] protected bool findFlag = false;
	[SerializeField] protected CheckPlayer checkPlayerScript;

	public override bool IsActive(bool reCheck = false) {
		if(checkPlayerScript != null) {
			findFlag = checkPlayerScript.playerStay;
			if (findFlag)
				alreadyFind = true;
		}

		if (reCheck)
			alreadyFind = findFlag;

		if (alreadyFind)
			return true;

		return false;
	}

	public override void Reset(bool newValue = false) {
		findFlag = false;
		alreadyFind = newValue;
	}


}
