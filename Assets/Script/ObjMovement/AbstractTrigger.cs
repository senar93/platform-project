﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractTrigger : MonoBehaviour {

	protected bool alreadyFind = false;

	public abstract bool IsActive(bool reCheck = false);
	public abstract void Reset(bool newValue = false);

}
