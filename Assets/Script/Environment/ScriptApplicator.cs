﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script che permette di applicare al giocatore un altro script di tipo AbstractPlayerSpecialAbility quando entra nel area
/// - lo script da applicare DEVE prima essere assegnato al oggetto, disabilitato e successivamente assegnato al puntatore
/// </summary>
public class ScriptApplicator : MonoBehaviour {

	public bool destroyFlag = false;
	[SerializeField] protected ReAbilitator reAbilitator;

	/// <summary>
	/// puntatore allo script da assegnare all oggetto
	/// - lo script da applicare DEVE prima essere assegnato al oggetto, disabilitato e successivamente assegnato al puntatore
	/// </summary>
	public AbstractPlayerSpecialAbility scriptToApply;
	/// <summary>
	/// attiva o disattiva la disabilitazione dell assegnamento dello script e della grafica del oggetto (diventa invisibile) dopo che ha applicato lo script
	/// </summary>
	public bool disableWhenApplied = true;

	/// <summary>
	/// attiva o disattiva l'assegnazione dello script al giocatore quando entra nel area
	/// </summary>
	protected bool enableEffect = true;

	/// <summary>
	/// assegna lo script al giocatore quanto entra nel area, 
	/// l'oggetto che identifica il giocatore DEVE avere il tag Player
	/// </summary>
	/// <param name="other">oggetto entrato nel area</param>
	void OnTriggerEnter2D(Collider2D other) {
		if (enableEffect && other.gameObject.tag == "Player" && scriptToApply != null &&
			other.gameObject.GetComponent(scriptToApply.GetType()) == null)
		{
			//istanzia al giocatore un nuovo script dello stesso tipo di quello di partenza
			other.gameObject.AddComponent(scriptToApply.GetType());
			//copia i valori dallo script di partenza a quello del giocatore
			(other.gameObject.GetComponent(scriptToApply.GetType()) as AbstractPlayerSpecialAbility).GetParameterFromScript(scriptToApply);

			if (disableWhenApplied)
				if (destroyFlag)
					DestroyEffect();
				else
					DisableEffect();
		}
	}


	/// <summary>
	/// abilita questo oggetto
	/// </summary>
	public void EnableEffect() {
		this.GetComponent<SpriteRenderer>().enabled = true;
		this.GetComponent<CircleCollider2D>().enabled = true;
		enableEffect = true;
	}

	/// <summary>
	/// disabilita questo oggetto
	/// </summary>
	public void DisableEffect() {
		this.GetComponent<SpriteRenderer>().enabled = false;
		this.GetComponent<CircleCollider2D>().enabled = false;
		enableEffect = false;
		if(reAbilitator != null)
			reAbilitator.enabled = true;
	}


	public void DestroyEffect() {
		Destroy(this.gameObject);
	}

}
