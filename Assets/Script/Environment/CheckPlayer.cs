﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPlayer : MonoBehaviour {

	public bool playerEnter = false;
	public bool playerStay = false;
	public bool playerExit = false;

	private bool previousFrameEnter = false;
	private bool previousFrameExit = false;


	void OnTriggerStay2D(Collider2D other) {
		if(other.tag == "Player")
			playerStay = true;
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.tag == "Player")
			playerEnter = true;
	}

	void OnTriggerExit2D(Collider2D other) {
		if (other.tag == "Player") {
			playerExit = true;
			playerStay = false;
		}
	}



	void Update() {
		if (playerEnter) {
			if (previousFrameEnter) {
				playerEnter = false;
				previousFrameEnter = false;
			}
			else {
				previousFrameEnter = true;
			}
		}

		if (playerExit) {
			if (previousFrameExit) {
				playerExit = false;
				previousFrameExit = false;
			}
			else {
				previousFrameExit = true;
			}
		}

	}




}
