﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyBlock : MonoBehaviour {

	public bool destroyFlag = false;
	[SerializeField] protected ReAbilitator reAbilitator;

	public bool disableByTimerAbsolute = false;
	public Timer expiredAbsolute = new Timer(60, 0);
	public bool disableByTimerRelative = false;
	public Timer expiredRelative = new Timer(10, 0);
	public bool disableByUse = true;
	public int useRemain = 5;

	[SerializeField] protected CheckPlayer checkPlayer;
	protected bool playerEnterFlag = false;
	protected int baseUseRemain;


	public void EnableObject() {
		this.GetComponent<SpriteRenderer>().enabled = true;
		this.GetComponent<Collider2D>().enabled = true;
		this.GetComponent<PlatformEffector2D>().enabled = true;
		checkPlayer.enabled = true;

		this.GetComponent<DestroyBlock>().enabled = true;
	}

	public void DisableObject() {
		this.GetComponent<SpriteRenderer>().enabled = false;
		this.GetComponent<Collider2D>().enabled = false;
		this.GetComponent<PlatformEffector2D>().enabled = false;
		checkPlayer.enabled = false;
		if (reAbilitator != null)
			reAbilitator.enabled = true;
		
		this.GetComponent<DestroyBlock>().enabled = false;
	}

	public void DestroyObject() {
		Destroy(this.gameObject);
	}

	public void Reset() {
		playerEnterFlag = false;
		expiredAbsolute.Reset();
		expiredRelative.Reset();
		useRemain = baseUseRemain;
	}


	void Awake() {
		baseUseRemain = useRemain;
	}


	// Update is called once per frame
	void Update () {

		bool disableFlag = false;
		//controlla se l'oggetto va disabilitato
		if (disableByUse && useRemain <= 0)
			disableFlag = true;
		else if (disableByTimerAbsolute && expiredAbsolute.Check())
			disableFlag = true;
		else if (disableByTimerRelative && expiredRelative.Check())
			disableFlag = true;
		
		//disabilita o distrugge l'oggetto
		if (disableFlag)
			if (destroyFlag)
				DestroyObject();
			else
				DisableObject();
		
		//avanza i timer e i contatori
		if(checkPlayer.playerStay) 
			expiredRelative.Advance();
		if (checkPlayer.playerExit)
			useRemain--;
		if (checkPlayer.playerEnter && !playerEnterFlag) {
			playerEnterFlag = true;
			expiredAbsolute.Advance();
		} else if (playerEnterFlag)
			expiredAbsolute.Advance();
		
	}

}
