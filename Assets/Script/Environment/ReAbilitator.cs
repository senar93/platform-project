﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReAbilitator : MonoBehaviour {

	public Timer timer = new Timer(10, 0);
	public MonoBehaviour script;
	
	// Update is called once per frame
	void Update () {
		timer.Advance();
		if(timer.Check()) {
			/* è necessario controlla per ogni tipo di script riattivabile da ReAbilitator che sia di una determinata tipologia
			 * e agire in base a ogni possibile tipo di script che gestisce l'oggetto riabilitabile
			 */
			if (script is DestroyBlock) {
				(script as DestroyBlock).Reset();
				(script as DestroyBlock).EnableObject();
			} else if (script is ScriptApplicator) {
				(script as ScriptApplicator).EnableEffect();
			}
			
			this.enabled = false;
		}
	}
}
